Canabae Ulpia Victrix
=====================

*__Note:__ This is my submission for the city plan portion of the Design Your Own Roman City project for [Yale's Roman Architecture course on Coursera](https://www.coursera.org/course/romanarchitecture) with Diana E. E. Kleiner.*

Scenario
--------

The year is 110 AD. I am bidding to build a canabae, or civilian settlement, around a military camp on the Marisus River in the Dacia Apulensis region of Dacia. The camp is overseen by the legate of the Legio XIII Gemina, who is also the administrator of the region — as was common in this era — and patron of this project.

This legion was historically based at the time in Apulum and controlled all of Dacia Apulensis, with subunits garrisoned in Germisara, Micia, and Ampelum. Settlements growing up around military camps were common throughout the Roman Empire and in Dacia, all of the major towns developed this way. So, the camp and settlement around it, though imagined, could have existed historically.

Site
----

The castrum sits across the river Marisus from the road connecting Apulum to the northeast and Micia to the west. There is plenty of fresh water from the river, timber and game from the forests, and beautiful, flat farmland all around that is perfect for building and farming. The site is just south of the wonderful hot springs at Germisara and the gold and silver mines at the city of Ampelum. Trade would have been easy due to proximity to a major road, the river, Apulum, and the mines.

[Here is a view of the modern day route](http://bit.ly/1oY4I0j) between Apulum, Germisara, and Micia. [Here is a closer view](http://bit.ly/1sTzvL1) of the site itself and [here is a map](http://bit.ly/1gTJiat) of Dacia at the time of this proposal.

Proposal
--------

I propose to build the canabae Ulpia Victrix, named in honor of the emperor’s gens and victory over the Dacians, around the existing castrum on the river Marisus south of Germisara. The town will accommodate 10,000 (approximately 2,500 homes) comfortably and still have room to grow into a colonia. The town would be divided into four main quarters.

I would make the area east of the camp, between the camp and the river, the trade and crafts district by extending the camp’s via principalis east and building wharves, warehouses, craftsmen's shops, a mill, a lumber yard and a bath.

The camp’s via principalis would be extended west, becoming the town’s decumanus.

The town’s cardo, which will be lined with vendors and shops, will extend north from the decumanus and at their junction will be the forum, temples, library and theater. See the plan for more of the town's features.

The camp’s via praetoria, which already runs south to the bridge, would be lined with shops, a bath, inns, and a brothel for travelers, soldiers, and citizens.

Building materials will be local mountain stone, cement, limestone, and timber - all used in Dacia.

Plan
----

[Plan of Canabae Ulpia Victrix](http://bit.ly/1govbhD)
